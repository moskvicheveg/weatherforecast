    var data = [
      {
        date: 1548014400000,
        temperature: {
        night: -1,
        day: 2,
        },
        cloudiness: 'Ясно',
        snow: false,
        rain: false,
        },
        {
      date: 1548100800000,
      temperature: {
      night: -2,
      day: 3,
      },
      cloudiness: 'Ясно',
      snow: false,
      rain: false,
    },

      {
      date: 1548187200000,
      temperature: {
      night: -3,
      day: 2,
      },
      cloudiness: 'Ясно',
      snow: false,
      rain: false,
      },
      {
      date: 1548273600000,
      temperature: {
      night: 0,
      day: 4,
      },
      cloudiness: 'Облачно',
      snow: false,
      rain: true,
      },
      {
      date: 1548360000000,
      temperature: {
      night: 0,
      day: 1,
      },
      cloudiness: 'Облачно',
      snow: true,
      rain: true,
      },
      {
      date: 1548446400000,
      temperature: {
        night:2,
        day: 3,
      },
      cloudiness: 'Облачно',
      snow: true,
      rain: true,
      },
      {
        date: 1548532800000,
        temperature: {
        night: -3,
        day: 1,
      },
      cloudiness: 'Облачно',
      snow: true,
      rain: true,
      }  
      ];

    //дата без времени на сегодня
    var currentDate;

    //индекс элемента массива для первой карточки
    var indexForFirstCard;

    function init(){
        //Устанавливаем текущую дату без времени
        currentDate = new Date(new Date(Date.now()).toDateString());
      
        // определяем индекс элемента с текущей датой getIndexWithToday      
        var index = data.findIndex(getIndexForecastByCurrentDate);
      
        // устанавливаем индекс для первой карты. ПРоцесс установки запускает процесс отрисовки
        setFirstCardIndex(index);
    }
    
    function leftShift(){
        setFirstCardIndex(--indexForFirstCard);

    }

    function rightShift(){
        setFirstCardIndex(++indexForFirstCard);             
    }

      //установка индекса для первой карты. Процесс установки запускает процесс отрисовки
      //проверка, что данные допустимы:
      //индекс не отрицательный
      //дата не меньше текущей
      //индекс не выходит за пределы data
    function setFirstCardIndex(index){
       console.log('setFirstCardIndex->index' + index);
       var cardsLength = getCards().length;
      //карты не перелистываются за пределы data 
       if (data.length - index >= cardsLength) {
          if (index >= 0) {
          indexForFirstCard = index;
          displayAllCards();  
         }
       else {
          index = 0;
         }
       }
       else {
          indexForFirstCard = data.length - cardsLength;
         }
    }      
        //Фильтр, возвращает данные прогноза из массива соответсвующие текущей дате
    function getIndexForecastByCurrentDate(element, index, array){
        return element.date == currentDate.valueOf(); 
    }    
      //запусукает процесс отображения данных на всех картах
    function displayAllCards(){
        var cards = getCards();
      
      console.log('indexForFirstCard'+ indexForFirstCard);
      for (var i = 0; i < cards.length; i++) {
        displayCard(cards[i], data[indexForFirstCard + i]);
      }
    }
      //Возвращает элементы карт для отображения
    function getCards(){
       return document.getElementsByClassName('card__block'); 
    }
      //Отображает в переданную карту переданные данные прогноза
    function displayCard(card, cardData){
      console.log(cardData);
        
    var date = new Date(cardData.date);
        
      //set day week
    var weekdayContent = (currentDate.valueOf() == date.valueOf() ? 'сегодня' : date.toLocaleString("ru",{weekday: 'long'}));
    card.querySelector(".day-card__dayweek").textContent = weekdayContent;

      //set data        
    card.querySelector(".day-card__data").textContent = date.toLocaleString("ru",{day: 'numeric', month:'long'});
        
      //set day temperature
    card.querySelector(".day-card__day-temperature__value").textContent = getTemperatureValue(cardData.temperature.day);

      //set night temperature
    card.querySelector(".day-card__night-temperature__value").textContent = getTemperatureValue(cardData.temperature.night);
       
      //set cloudiness
    card.querySelector(".day-card__cloudiness__value").textContent = getCloudinessValue(cardData.cloudiness);     
    
      //set snow
      //card.querySelector(".day-card__snow__value").textContent = getSnowValue(cardData.Snow.day);

      //set rain
      //card.querySelector(".day-card__rain__value").textContent = getRainValue(cardData.Rain.day);
    }

    function getTemperatureValue(value){
      var sign = value>0 ? '+' : '';
      return sign + value + String.fromCharCode(176);
    }

    function getCloudinessValue(value) {
      var sing = value>0 ? '+' : '';
      return sing + value + String.fromCharCode();
    }